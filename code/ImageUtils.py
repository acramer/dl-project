import numpy as np

"""This script implements the functions for data augmentation
and preprocessing.
"""

def parse_record(record, training):
    """Parse a record to an image and perform data preprocessing.

    Args:
        record: An array of shape [3072,]. One row of the x_* matrix.
        training: A boolean. Determine whether it is in training mode.

    Returns:
        image: An array of shape [32, 32, 3].
    """
    ### YOUR CODE HERE

    image = record.reshape(32,32,3)

    ### END CODE HERE

    image = preprocess_image(image, training) # If any.

    return image


def preprocess_image(image, training):
    """Preprocess a single image of shape [height, width, depth].

    Args:
        image: An array of shape [32, 32, 3].
        training: A boolean. Determine whether it is in training mode.

    Returns:
        image: An array of shape [32, 32, 3]. The processed image.
    """
    ### YOUR CODE HERE

    # Data augmentation is handled in training by pytorch transformations

    ### END CODE HERE

    return image


# Other functions
### YOUR CODE HERE
from PIL import Image
from torch.utils.data import Dataset
from torchvision import transforms
from torchvision.transforms import functional as F

class ImageDataset(Dataset):
    def __init__(self, x_data, label_data, shape=(32,32,3), transform=transforms.Compose([transforms.ToTensor()])):
        self.dataset = list(zip(x_data.reshape(-1,*shape), label_data))
        self.transform = transform
        if isinstance(x_data, np.ndarray):
            self.transform.transforms.insert(0, transforms.ToPILImage())

    def __getitem__(self, index):
        return self.transform(self.dataset[index][0]), self.dataset[index][1]

    def __len__(self):
        return len(self.dataset)

### END CODE HERE
