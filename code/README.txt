Cifar Version: python

Dataset Tree Structure: ResNet/cifar/cifar-10-batches-py/*

    ./data/
           cifar-10-batches-py/
                               data_batch_1
                               data_batch_2
                               data_batch_3
                               data_batch_4
                               data_batch_5
                               test_batch
Private Test Data Structure:

    ./test/private_test_images.npy


Instructions:
    Testing saved model:
            - python main.py --mode test --load saved_models/model.th

    Prediction with saved model:
            - python main.py --mode predict --load saved_models/model.th


Traing and Testing new models:
    All recently trained models will be saved in models folder under the largest numbered model.
        - the -S 0 flag and argument specifies that the model should be saved at the end of training.
        - the -S N flag and argument specifies that the model should be saved every N epochs, into the
                same folder, with the name modeli<E>.th, where <E> is the epoch number.
        - to resume training on a model, simply specify model.th with --load <path/to/model.th> flag and argument.

    List Training Hyper-parameter:
            - python main.py -h

    Training DLA-34:
            - python main.py -n -A dla

    Training DLA-SE-34:
            - python main.py -n -A dla -e

    Training DLA-34 to best accuracy:
            - python main.py -S 0 -nsdE 120 -B 128 -L 0.01 -A dla --Act swish

    Training Best Model (DLA-SE-34 + ADA + Dropout rate 0.3) to best accuracy:
            - python main.py -S 0 -nsdE 150 -B 128 -L 0.01 -A dla --Act swish -eD 0.3  --Aug all

    Testing first model run locally:
            - python main.py --mode test --load models/0/model.th

    Testing first checkpoint of first model run locally (assuming checkpoints taken every 5 epochs '-S 5' flag):
            - python main.py --mode test --load models/0/modeli5.th

    Prediction first model run locally:
            - python main.py --mode predict --load models/0/model.th
