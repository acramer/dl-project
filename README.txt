TODO

* Idea for 'make gpu-kill and make save-model' : call gcloud to get a list of machines that are on, get ip address of alan.  use scp to change file at base.
* make requirements.txt
* make environment.yml
* 'make Andrew_Cramer.zip' creates a folder Andrew_Cramer/, then adds files to folder, then zips with -rm flags on that folder
* 'make test' checks the correctness of the code with test.py in code.
* 'make venv' builds the environment with venv + requirements.txt
* 'make conda' builds the environment with conda + environment.yml
* Test 'make venv' and 'make conda' on clean machines (both cpu and gpu)
* Populate this file with instructions to run specified in the Project pdf
* Populate this file with instructions to run using the makefile
